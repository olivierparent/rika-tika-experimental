Rika-Tika v2
============

Domeinnaam mapping
------------------

In `Homestead.yaml` voeg je de mapping toe:

```
# Rika-Tika
# ---------
    - map: v1.clocklearning.arteveldehogeschool.local
      to: /home/vagrant/Code/clocklearning.arteveldehogeschool.local/v1/public

    - map: www.clocklearning.arteveldehogeschool.local
      to: /home/vagrant/Code/clocklearning.arteveldehogeschool.local/v2/web
      sh: symfony_prod

    - map: dev.clocklearning.arteveldehogeschool.local
      to: /home/vagrant/Code/clocklearning.arteveldehogeschool.local/v2/web
      sh: symfony_dev
```

Vagrant starten met de nieuwe provision scripts:

    $ vagrant up --provision

Project clonen
--------------

    $ vagrant ssh
    vagrant@homestead$ mkdir -p ~/Code/clocklearning.arteveldehogeschool.local/
    vagrant@homestead$ cd ~/Code/clocklearning.arteveldehogeschool.local/
    vagrant@homestead$ git clone https://bitbucket.org/olivierparent/rika-tika-experimental v2/
    vagrant@homestead$ cd v2/
    vagrant@homestead$ sudo composer self-update
    vagrant@homestead$ composer update
    vagrant@homestead$ php app/console --version

Database
--------

    vagrant@homestead$ mysql --user=homestead --password=secret --execute="GRANT ALL PRIVILEGES ON v2_clocklearning_arteveldehogeschool_be.* TO 'rika-tika' IDENTIFIED BY 'clocklearning'"
    vagrant@homestead$ php app/console doctrine:database:create
    vagrant@homestead$ php app/console doctrine:schema:create
    vagrant@homestead$ php app/console doctrine:fixtures:load

Test installatie
----------------

 - http://dev.clocklearning.arteveldehogeschool.local/
 - http://www.clocklearning.arteveldehogeschool.local/
