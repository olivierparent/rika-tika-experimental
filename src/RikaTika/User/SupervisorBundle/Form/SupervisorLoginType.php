<?php

namespace RikaTika\User\SupervisorBundle\Form;

use RikaTika\CoreBundle\Form\SupervisorType;
use Symfony\Component\Form\FormBuilderInterface;

class SupervisorLoginType extends SupervisorType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username')
            ->add('password', 'password')
            ->add('btn_login', 'submit', [
                'label' => 'Sign on',
            ])
        ;
    }

    /**
     * Form name.
     *
     * @return string
     */
    public function getName()
    {
        return 'login';
    }
} 
