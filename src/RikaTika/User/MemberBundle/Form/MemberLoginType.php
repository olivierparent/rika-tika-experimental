<?php

namespace RikaTika\User\MemberBundle\Form;

use RikaTika\CoreBundle\Form\MemberType;
use Symfony\Component\Form\FormBuilderInterface;

class MemberLoginType extends MemberType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username')
            ->add('password', 'password')
            ->add('btn_login', 'submit', [
                'label' => 'Sign on',
            ])
        ;
    }

    /**
     * Form name.
     *
     * @return string
     */
    public function getName()
    {
        return 'login';
    }
} 
