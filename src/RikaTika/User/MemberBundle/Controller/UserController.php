<?php

namespace RikaTika\User\MemberBundle\Controller;

use RikaTika\CoreBundle\Entity\Member;
use RikaTika\User\MemberBundle\Form\MemberLoginType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class UserController extends Controller
{
    /**
     * @Route("/login")
     * @Template()
     *
     * @param Request $request
     * @return array
     */
    public function loginAction(Request $request)
    {
        $entity   = new Member();
        $formType = new MemberLoginType();

        $form = $this->createForm($formType, $entity, [
            'action' => $this->generateUrl('rikatika_user_member_user_check')
        ]);

        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
        } else {
            $session = $request->getSession();
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
        }

        return [
            'form'  => $form->createView(),
            'error' => $error,
        ]; // Return array with variables for Twig.
    }
}
