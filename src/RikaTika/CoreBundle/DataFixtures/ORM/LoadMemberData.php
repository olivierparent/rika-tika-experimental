<?php

namespace RikaTika\CoreBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use RikaTika\CoreBundle\Entity\Member;

/**
 * LoadMemberData
 *
 * @category RikaTika
 * @package CoreBundle
 * @subpackage DataFixtures\ORM
 * @author Olivier Parent <olivier.parent@arteveldehs.be>
 * @copyright Copyright © 2011-2014, Artevelde University College Ghent
 * @license http://www.clocklearning.org/LICENSE.txt
 */
class LoadMemberData extends AbstractFixture implements DependentFixtureInterface
{
    /**
     * {@inheritdoc}
     */
    public function getDependencies()
    {
        return [
            __NAMESPACE__ . '\LoadLanguageData',
            __NAMESPACE__ . '\LoadMemberGroupData',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $entityManager)
    {
        $member = new Member();
        $member
            ->setFirstName('Member')
            ->setLastName('Fixture')
            ->setUsername('member')
            ->setPassword('member')
            ->setLanguage($this->getReference('languageNL'))
            ->setMemberGroup($this->getReference('memberGroup'))
            ->setBirthday(new \DateTime());
        $entityManager->persist($member); // Manage Entity for persistence.
        $this->addReference('member', $member); // Reference for the next Data Fixture(s).

        $entityManager->flush(); // Persist all managed objects.
    }
}
