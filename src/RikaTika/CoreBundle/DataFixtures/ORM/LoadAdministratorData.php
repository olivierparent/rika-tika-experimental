<?php

namespace RikaTika\CoreBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use RikaTika\CoreBundle\Entity\Administrator;

/**
 * LoadAdministratorData
 *
 * @category RikaTika
 * @package CoreBundle
 * @subpackage DataFixtures\ORM
 * @author Olivier Parent <olivier.parent@arteveldehs.be>
 * @copyright Copyright © 2011-2014, Artevelde University College Ghent
 * @license http://www.clocklearning.org/LICENSE.txt
 */
class LoadAdministratorData extends AbstractFixture implements DependentFixtureInterface
{
    /**
     * {@inheritdoc}
     */
    public function getDependencies()
    {
        return [
            __NAMESPACE__ . '\LoadLanguageData',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $entityManager)
    {
        // Administrator
        $administrator = new Administrator();
        $administrator
            ->setFirstName('Administrator')
            ->setLastName('Fixture')
            ->setUsername('administrator')
            ->setPassword('administrator')
            ->setLanguage($this->getReference('languageNL'))
            ->setEmail('administrator@arteveldehs.be');
        $entityManager->persist($administrator); // Manage Entity for persistence.
        $this->addReference('adminstrator', $administrator); // Reference for the next Data Fixture(s).

        $entityManager->flush(); // Persist all managed objects.
    }
}
