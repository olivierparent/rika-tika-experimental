<?php

namespace RikaTika\CoreBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use RikaTika\CoreBundle\Entity\Supervisor;

/**
 * LoadSupervisorData
 *
 * @category RikaTika
 * @package CoreBundle
 * @subpackage DataFixtures\ORM
 * @author Olivier Parent <olivier.parent@arteveldehs.be>
 * @copyright Copyright © 2011-2014, Artevelde University College Ghent
 * @license http://www.clocklearning.org/LICENSE.txt
 */
class LoadSupervisorData extends AbstractFixture implements DependentFixtureInterface
{
    /**
     * {@inheritdoc}
     */
    public function getDependencies()
    {
        return [
            __NAMESPACE__ . '\LoadLanguageData',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $entityManager)
    {
        // Supervisor
        $supervisor = new Supervisor();
        $supervisor
            ->setFirstName('Supervisor')
            ->setLastName('Fixture')
            ->setUsername('supervisor')
            ->setPassword('supervisor')
            ->setLanguage($this->getReference('languageNL'))
            ->setProfession(Supervisor::PROFESSION_THERAPIST);
        $entityManager->persist($supervisor); // Manage Entity for persistence.
        $this->addReference('supervisor', $supervisor); // Reference for the next Data Fixture(s).

        $entityManager->flush(); // Persist all managed objects.
    }
}
