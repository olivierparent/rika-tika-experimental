<?php

#
# $ phpunit -c app/ src/RikaTika/CoreBundle/Tests/Library/TimeGenerator/TimeGeneratorDigitalAnswerModeTest.php
#

namespace RikaTika\CoreBundle\Tests\Library\TimeGenerator;

use RikaTika\CoreBundle\Entity\ExerciseSettings;
use RikaTika\CoreBundle\Library\TimeGenerator\TimeGeneratorAbstract;
use RikaTika\CoreBundle\Library\TimeGenerator\TimeGeneratorEN;
use RikaTika\CoreBundle\Library\TimeGenerator\TimeGeneratorNL;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * TimeGeneratorDigitalAnswerModeTest
 *
 * @see http://phpunit.de/manual/current/en/appendixes.assertions.html
 *
 * @category RikaTika
 * @package CoreBundle
 * @subpackage Tests\Library\TimeGenerator
 * @author Olivier Parent <olivier.parent@arteveldehs.be>
 * @copyright Copyright © 2011-2014, Artevelde University College Ghent
 * @license http://www.clocklearning.org/LICENSE.txt
 */
class TimeGeneratorDigitalAnswerModeTest extends WebTestCase
{
    /**
     *
     */
    public function testErrorDigitalEarlierBy30M()
    {
    }

    /**
     *
     */
    public function testErrorDigitalEarlierBy60M()
    {
    }

    /**
     *
     */
    public function testErrorDigitalReadH×10()
    {
    }

    /**
     *
     */
    public function testErrorDigitalReadH×100()
    {
    }

    /**
     *
     */
    public function testErrorDigitalReadHour()
    {
    }

    /**
     *
     */
    public function testErrorDigitalReadM00As0()
    {
    }

    /**
     *
     */
    public function testErrorDigitalReadReverseMDigits()
    {
    }

    /**
     *
     */
    public function testErrorDigitalReadQuartersAsNumber()
    {
    }

    /**
     *
     */
    public function testErrorDigitalReadSwapHAndM()
    {
    }

    /**
     *
     */
    public function testErrorDigitalReadSwapHAndMThenPast()
    {
    }

    /**
     *
     */
    public function testErrorDigitalReadSwapHAndMThenReverseHDigits()
    {
    }

    /**
     *
     */
    public function testErrorDigitalReadTo()
    {
    }

    /**
     *
     */
    public function testErrorDigitalSwapPastAndTo()
    {
    }

    /**
     *
     */
    public function testErrorDigitalSwapPastAndToThenReadQuartersAsNumber()
    {
    }
}
