<?php

#
# $ phpunit -c app/ src/RikaTika/CoreBundle/Tests/Library/TimeGenerator/TimeGeneratorTest.php
#

namespace RikaTika\CoreBundle\Tests\Library\TimeGenerator;

use RikaTika\CoreBundle\Entity\ExerciseSettings;
use RikaTika\CoreBundle\Library\TimeGenerator\TimeGeneratorAbstract;
use RikaTika\CoreBundle\Library\TimeGenerator\TimeGeneratorEN;
use RikaTika\CoreBundle\Library\TimeGenerator\TimeGeneratorNL;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Validator\Constraints\Time;

/**
 * TimeGeneratorTest
 *
 * @see http://phpunit.de/manual/current/en/appendixes.assertions.html
 *
 * @category RikaTika
 * @package CoreBundle
 * @subpackage Tests\Library\TimeGenerator
 * @author Olivier Parent <olivier.parent@arteveldehs.be>
 * @copyright Copyright © 2011-2014, Artevelde University College Ghent
 * @license http://www.clocklearning.org/LICENSE.txt
 */
class TimeGeneratorTest extends WebTestCase
{
    /**
     * Test: Module settings
     */
    public function testModuleSettings()
    {
        // 12-hour clock
        TimeGeneratorAbstract::getModuleSettings()->setHasTwentyFour(false);
        $this->assertFalse(TimeGeneratorAbstract::getModuleSettings()->getHasTwentyFour());
        $this->assertFalse(TimeGeneratorEN::getModuleSettings()->getHasTwentyFour());
        $this->assertFalse(TimeGeneratorNL::getModuleSettings()->getHasTwentyFour());

        $time = new TimeGeneratorEN();
        $this->assertFalse($time::getModuleSettings()->getHasTwentyFour());

        $time = new TimeGeneratorNL();
        $this->assertFalse($time::getModuleSettings()->getHasTwentyFour());

        // 24-hour clock
        TimeGeneratorAbstract::getModuleSettings()->setHasTwentyFour(true);
        $this->assertTrue(TimeGeneratorAbstract::getModuleSettings()->getHasTwentyFour());
        $this->assertTrue(TimeGeneratorEN::getModuleSettings()->getHasTwentyFour());
        $this->assertTrue(TimeGeneratorNL::getModuleSettings()->getHasTwentyFour());

        $time = new TimeGeneratorNL();
        $this->assertTrue($time::getModuleSettings()->getHasTwentyFour());

        $time = new TimeGeneratorEN();
        $this->assertTrue($time::getModuleSettings()->getHasTwentyFour());
    }

    /**
     * Test TimeGeneratorAbstract method toTime()
     */
    public function testToTimeNL()
    {
        // 24-hour clock & 12-hour clock

        // 24-hour clock
        TimeGeneratorAbstract::getModuleSettings()->setHasTwentyFour(true);
        list ($hMin, $hMax) = TimeGeneratorAbstract::getHourRange();

        $time = new TimeGeneratorNL();
        $this->assertSame('00:00', $time->toTime());

        $this->assertSame( 0, $hMin);
        $this->assertSame(23, $hMax);
        for ($h = $hMin; $h <= $hMax; ++$h) {
            $time->setTime($h);
            $this->assertSame(sprintf('%02d:00', $h), $time->toTime());
        }

        // 24-hour clock → 12-hour clock
        TimeGeneratorAbstract::getModuleSettings()->setHasTwentyFour(true);
        $this->assertTrue($time::getModuleSettings()->getHasTwentyFour());
        $time->setTime();
        $this->assertSame('00:00', $time->toTime());
        TimeGeneratorAbstract::getModuleSettings()->setHasTwentyFour(false);
        $this->assertFalse($time::getModuleSettings()->getHasTwentyFour());
        $this->assertSame('12:00', $time->toTime());

        TimeGeneratorAbstract::getModuleSettings()->setHasTwentyFour(true);
        $this->assertTrue(TimeGeneratorNL::getModuleSettings()->getHasTwentyFour());
        $time->setTime(13);
        $this->assertSame('13:00', $time->toTime());
        TimeGeneratorAbstract::getModuleSettings()->setHasTwentyFour(false);
        $this->assertFalse($time::getModuleSettings()->getHasTwentyFour());
        $this->assertSame('01:00', $time->toTime());

        TimeGeneratorAbstract::getModuleSettings()->setHasTwentyFour(true);
        $this->assertTrue($time::getModuleSettings()->getHasTwentyFour());
        $time->setTime(23);
        $this->assertSame('23:00', $time->toTime());
        TimeGeneratorAbstract::getModuleSettings()->setHasTwentyFour(false);
        $this->assertFalse($time::getModuleSettings()->getHasTwentyFour());
        $this->assertSame('11:00', $time->toTime());

        // 12-hour clock
        TimeGeneratorAbstract::getModuleSettings()->setHasTwentyFour(false);
        $this->assertFalse($time::getModuleSettings()->getHasTwentyFour());
        list ($hMin, $hMax) = TimeGeneratorAbstract::getHourRange();

        $time = new TimeGeneratorNL();
        $this->assertSame('12:00', $time->toTime());

        $this->assertSame( 1, $hMin);
        $this->assertSame(12, $hMax);
        for ($h = $hMin; $h <= $hMax; ++$h) {
            for ($h = $hMin; $h <= $hMax; ++$h) {
                $time->setTime($h);
                $this->assertSame(sprintf('%02d:00', $h), $time->toTime());
            }
        }
    }

    public function testToTextNL()
    {
        // 24-hour clock & 12-hour clock
        list ($mMin, $mMax) = TimeGeneratorAbstract::getMinuteRange();
        $this->assertSame( 0, $mMin);
        $this->assertSame(59, $mMax);

        // 24-hour clock
        TimeGeneratorAbstract::getModuleSettings()->setHasTwentyFour(true);
        $this->assertTrue(TimeGeneratorAbstract::getModuleSettings()->getHasTwentyFour());
        list ($hMin, $hMax) = TimeGeneratorAbstract::getHourRange();
        $this->assertSame( 0, $hMin);
        $this->assertSame(23, $hMax);

        $time = new TimeGeneratorNL();

        for ($h = $hMin; $h <= $hMax; ++$h) {

            for ($m = $mMin; $m <= $mMax; ++$m) {
                $time->setTime($h, $m);
                $this->assertSame(sprintf('%02d:%02d', $h, $m), $time->toTime());

                // Official clock system
                $time::getModuleSettings()->setHasQuadrants(true);
                $this->assertTrue($time::getModuleSettings()->getHasQuadrants());
                switch ($m) {
                    case  1:
                        $this->assertStringMatchesFormat('1 minuut over %d', $time->toText());
                        break;
                    case 14:
                        $this->assertStringMatchesFormat('14 minuten over %d', $time->toText());
                        break;
                    case 15:
                        $this->assertStringMatchesFormat('kwart over %d', $time->toText());
                        break;
                    case 16:
                        $this->assertStringMatchesFormat('14 minuten voor half %d', $time->toText());
                        break;
                    case 29:
                        $this->assertStringMatchesFormat('1 minuut voor half %d', $time->toText());
                        break;
                    case 30:
                        $this->assertStringMatchesFormat('half %d', $time->toText());
                        break;
                    case 31:
                        $this->assertStringMatchesFormat('1 minuut over half %d', $time->toText());
                        break;
                    case 44:
                        $this->assertStringMatchesFormat('14 minuten over half %d', $time->toText());
                        break;
                    case 45:
                        $this->assertStringMatchesFormat('kwart voor %d', $time->toText());
                        break;
                    case 46:
                        $this->assertStringMatchesFormat('14 minuten voor %d', $time->toText());
                        break;
                    case 59:
                        $this->assertStringMatchesFormat('1 minuut voor %d', $time->toText());
                        break;
                    default:
                }

                // Alternative clock system
                TimeGeneratorNL::getModuleSettings()->setHasQuadrants(false);
                $this->assertFalse(TimeGeneratorNL::getModuleSettings()->getHasQuadrants());
                switch ($m) {
                    case  1:
                        $this->assertStringMatchesFormat('1 minuut over %d', $time->toText());
                        break;
                    case 14:
                        $this->assertStringMatchesFormat('14 minuten over %d', $time->toText());
                        break;
                    case 15:
                        $this->assertStringMatchesFormat('kwart over %d', $time->toText());
                        break;
                    case 16:
                        $this->assertStringMatchesFormat('16 minuten over %d', $time->toText());
                        break;
                    case 29:
                        $this->assertStringMatchesFormat('29 minuten over %d', $time->toText());
                        break;
                    case 30:
                        $this->assertStringMatchesFormat('half %d', $time->toText());
                        break;
                    case 31:
                        $this->assertStringMatchesFormat('29 minuten voor %d', $time->toText());
                        break;
                    case 44:
                        $this->assertStringMatchesFormat('16 minuten voor %d', $time->toText());
                        break;
                    case 45:
                        $this->assertStringMatchesFormat('kwart voor %d', $time->toText());
                        break;
                    case 46:
                        $this->assertStringMatchesFormat('14 minuten voor %d', $time->toText());
                        break;
                    case 59:
                        $this->assertStringMatchesFormat('1 minuut voor %d', $time->toText());
                        break;
                    default:
                }
            }
        }

        // 12-hour clock
        TimeGeneratorNL::getModuleSettings()->setHasTwentyFour(false);
        $this->assertFalse(TimeGeneratorNL::getModuleSettings()->getHasTwentyFour());
        list ($hMin, $hMax) = TimeGeneratorNL::getHourRange();
        $this->assertSame( 1, $hMin);
        $this->assertSame(12, $hMax);

        $time = new TimeGeneratorNL();

        for ($h = $hMin; $h <= $hMax; ++$h) {
            for ($m = $mMin; $m <= $mMax; ++$m) {
                $time->setTime($h, $m);
                $this->assertSame(sprintf('%02d:%02d', $h, $m), $time->toTime());

                // Official clock system
                $time::getModuleSettings()->setHasQuadrants(true);
                $this->assertTrue($time::getModuleSettings()->getHasQuadrants());
                switch ($m) {
                    case  1:
                        $this->assertStringMatchesFormat('1 minuut over %d', $time->toText());
                        break;
                    case 14:
                        $this->assertStringMatchesFormat('14 minuten over %d', $time->toText());
                        break;
                    case 15:
                        $this->assertStringMatchesFormat('kwart over %d', $time->toText());
                        break;
                    case 16:
                        $this->assertStringMatchesFormat('14 minuten voor half %d', $time->toText());
                        break;
                    case 29:
                        $this->assertStringMatchesFormat('1 minuut voor half %d', $time->toText());
                        break;
                    case 30:
                        $this->assertStringMatchesFormat('half %d', $time->toText());
                        break;
                    case 31:
                        $this->assertStringMatchesFormat('1 minuut over half %d', $time->toText());
                        break;
                    case 44:
                        $this->assertStringMatchesFormat('14 minuten over half %d', $time->toText());
                        break;
                    case 45:
                        $this->assertStringMatchesFormat('kwart voor %d', $time->toText());
                        break;
                    case 46:
                        $this->assertStringMatchesFormat('14 minuten voor %d', $time->toText());
                        break;
                    case 59:
                        $this->assertStringMatchesFormat('1 minuut voor %d', $time->toText());
                        break;
                    default:
                }

                // Alternative clock system
                TimeGeneratorNL::getModuleSettings()->setHasQuadrants(false);
                $this->assertFalse(TimeGeneratorNL::getModuleSettings()->getHasQuadrants());
                switch ($m) {
                    case  1:
                        $this->assertStringMatchesFormat('1 minuut over %d', $time->toText());
                        break;
                    case 14:
                        $this->assertStringMatchesFormat('14 minuten over %d', $time->toText());
                        break;
                    case 15:
                        $this->assertStringMatchesFormat('kwart over %d', $time->toText());
                        break;
                    case 16:
                        $this->assertStringMatchesFormat('16 minuten over %d', $time->toText());
                        break;
                    case 29:
                        $this->assertStringMatchesFormat('29 minuten over %d', $time->toText());
                        break;
                    case 30:
                        $this->assertStringMatchesFormat('half %d', $time->toText());
                        break;
                    case 31:
                        $this->assertStringMatchesFormat('29 minuten voor %d', $time->toText());
                        break;
                    case 44:
                        $this->assertStringMatchesFormat('16 minuten voor %d', $time->toText());
                        break;
                    case 45:
                        $this->assertStringMatchesFormat('kwart voor %d', $time->toText());
                        break;
                    case 46:
                        $this->assertStringMatchesFormat('14 minuten voor %d', $time->toText());
                        break;
                    case 59:
                        $this->assertStringMatchesFormat('1 minuut voor %d', $time->toText());
                        break;
                    default:
                }
            }
        }
    }

    /**
     *
     */
    public function testTimeSettersGettersNL()
    {
        // Initialize settings
        TimeGeneratorAbstract::setExerciseSettings();
        TimeGeneratorAbstract::setModuleSettings();

        // Enable seconds
        TimeGeneratorAbstract::getExerciseSettings()->setHasSeconds(true);
        $this->assertTrue(TimeGeneratorAbstract::getExerciseSettings()->getHasSeconds());
//        TimeGeneratorAbstract::getExerciseSettings()->setHasSeconds(false);
//        $this->assertFalse(TimeGeneratorAbstract::getExerciseSettings()->getHasSeconds());

        // 24-hour clock & 12-hour clock
        list ($sMin, $sMax) = TimeGeneratorAbstract::getSecondRange();
        $this->assertSame( 0, $sMin);
        $this->assertSame(59, $sMax);

        list ($mMin, $mMax) = TimeGeneratorAbstract::getMinuteRange();
        $this->assertSame( 0, $mMin);
        $this->assertSame(59, $mMax);

        // 24-hour clock
        TimeGeneratorAbstract::getModuleSettings()->setHasTwentyFour(true);
        $this->assertTrue(TimeGeneratorAbstract::getModuleSettings()->getHasTwentyFour());

        list ($hMin, $hMax) = TimeGeneratorAbstract::getHourRange();
        $this->assertSame( 0, $hMin);
        $this->assertSame(23, $hMax);

        $time = new TimeGeneratorNL();
        $time->setTime($hMin, $mMin, $sMin);
        $this->assertSame('00:00:00', $time->toTime());
        $this->assertSame(0, $time->getTimeInSeconds());

        $time = new TimeGeneratorNL();
        $time->setTimeInSeconds(0);
        $this->assertSame('00:00:00', $time->toTime());

        $time = new TimeGeneratorNL();
        $time->setTime(0, 0, 1);
        $this->assertSame('00:00:01', $time->toTime());
        $this->assertSame(1, $time->getTimeInSeconds());

        $time = new TimeGeneratorNL();
        $time->setTimeInSeconds(1);
        $this->assertSame('00:00:01', $time->toTime());

        $time = new TimeGeneratorNL();
        $time->setTime(0, 0, 2);
        $this->assertSame('00:00:02', $time->toTime());
        $this->assertSame(2, $time->getTimeInSeconds());

        $time = new TimeGeneratorNL();
        $time->setTimeInSeconds(2);
        $this->assertSame('00:00:02', $time->toTime());

        $time = new TimeGeneratorNL();
        $time->setTime(0, 0, 59);
        $this->assertSame('00:00:59', $time->toTime());
        $this->assertSame(59, $time->getTimeInSeconds());

        $time = new TimeGeneratorNL();
        $time->setTimeInSeconds(59);
        $this->assertSame('00:00:59', $time->toTime());

        $time = new TimeGeneratorNL();
        $time->setTime(0, 1, 0);
        $this->assertSame('00:01:00', $time->toTime());
        $this->assertSame(60, $time->getTimeInSeconds());

        $time = new TimeGeneratorNL();
        $time->setTimeInSeconds(60);
        $this->assertSame('00:01:00', $time->toTime());

        $time = new TimeGeneratorNL();
        $time->setTime(0, 59, 0);
        $this->assertSame('00:59:00', $time->toTime());
        $this->assertSame(3540, $time->getTimeInSeconds());

        $time = new TimeGeneratorNL();
        $time->setTimeInSeconds(3540);
        $this->assertSame('00:59:00', $time->toTime());

        $time = new TimeGeneratorNL();
        $time->setTime(0, 59, 59);
        $this->assertSame('00:59:59', $time->toTime());
        $this->assertSame(3599, $time->getTimeInSeconds());

        $time = new TimeGeneratorNL();
        $time->setTimeInSeconds(3599);
        $this->assertSame('00:59:59', $time->toTime());

        $time = new TimeGeneratorNL();
        $time->setTime(1, 0, 0);
        $this->assertSame('01:00:00', $time->toTime());
        $this->assertSame(TimeGeneratorAbstract::H_SECONDS, $time->getTimeInSeconds());

        $time = new TimeGeneratorNL();
        $time->setTimeInSeconds(3600);
        $this->assertSame('01:00:00', $time->toTime());

        $time = new TimeGeneratorNL();
        $time->setTime($hMax, $mMax, $sMax);
        $this->assertSame('23:59:59', $time->toTime());
        $this->assertSame(23 * TimeGeneratorAbstract::H_SECONDS + 59 * TimeGeneratorAbstract::M_SECONDS + 59, $time->getTimeInSeconds());

        $time = new TimeGeneratorNL();
        $time->setTimeInSeconds(23 * 3600 + 59 * 60 + 59);
        $this->assertSame('23:59:59', $time->toTime());

        // 12-hour clock
        TimeGeneratorAbstract::getModuleSettings()->setHasTwentyFour(false);
        $this->assertFalse(TimeGeneratorAbstract::getModuleSettings()->getHasTwentyFour());

        list ($hMin, $hMax) = TimeGeneratorAbstract::getHourRange();
        $this->assertSame( 1, $hMin);
        $this->assertSame(12, $hMax);

        $time = new TimeGeneratorNL();
        $time->setTime($hMin, $mMin, $sMin);
        $this->assertSame('01:00:00', $time->toTime());
        $this->assertSame(TimeGeneratorAbstract::H_SECONDS, $time->getTimeInSeconds());

        $time = new TimeGeneratorNL();
        $time->setTimeInSeconds(0);
        $this->assertSame('12:00:00', $time->toTime());

        $time = new TimeGeneratorNL();
        $time->setTime(12, 0, 1);
        $this->assertSame('12:00:01', $time->toTime());
        $this->assertSame(1, $time->getTimeInSeconds());

        $time = new TimeGeneratorNL();
        $time->setTime($hMax, $mMax, $sMax);
        $this->assertSame('12:59:59', $time->toTime());
        $this->assertSame(3599, $time->getTimeInSeconds());

        $time = new TimeGeneratorNL();
        $time->setTime(11, 59, 59);
        $this->assertSame('11:59:59', $time->toTime());
        $this->assertSame(11 * TimeGeneratorAbstract::H_SECONDS + 59 * TimeGeneratorAbstract::M_SECONDS + 59, $time->getTimeInSeconds());
    }

    public function testConversions()
    {
        // Initialize settings
        TimeGeneratorAbstract::setExerciseSettings();
        TimeGeneratorAbstract::setModuleSettings();

        // 24-hour clock
        TimeGeneratorAbstract::getModuleSettings()->setHasTwentyFour(true);
        $this->assertTrue(TimeGeneratorAbstract::getModuleSettings()->getHasTwentyFour());

        // 24-hour clock → 12-hour clock
        $time = new TimeGeneratorNL();
        $time->setTime(0);
        $this->assertSame(0, $time->getHour());
        $this->assertSame(12, $time->to12HourClock($time->getHour()));

        $time->setTime(1);
        $this->assertSame(1, $time->to12HourClock($time->getHour()));

        $time->setTime(11);
        $this->assertSame(11, $time->to12HourClock($time->getHour()));

        $time->setTime(12);
        $this->assertSame(12, $time->to12HourClock($time->getHour()));

        $time->setTime(13);
        $this->assertSame(1, $time->to12HourClock($time->getHour()));

        $time->setTime(23);
        $this->assertSame(11, $time->to12HourClock($time->getHour()));

        // 24-hour clock → 24-hour clock A.M.
        $time = new TimeGeneratorNL();
        $time->setTime(0);
        $this->assertSame(0, $time->getHour());
        $this->assertSame(0, $time->to24HourClockAnteMeridiem($time->getHour()));

        $time->setTime(1);
        $this->assertSame(1, $time->to24HourClockAnteMeridiem($time->getHour()));

        $time->setTime(11);
        $this->assertSame(11, $time->to24HourClockAnteMeridiem($time->getHour()));

        $time->setTime(12);
        $this->assertSame(0, $time->to24HourClockAnteMeridiem($time->getHour()));

        $time->setTime(13);
        $this->assertSame(1, $time->to24HourClockAnteMeridiem($time->getHour()));

        $time->setTime(23);
        $this->assertSame(11, $time->to24HourClockAnteMeridiem($time->getHour()));

        // 24-hour clock → 24-hour clock P.M.
        $time = new TimeGeneratorNL();
        $time->setTime(0);
        $this->assertSame(0, $time->getHour());
        $this->assertSame(12, $time->to24HourClockPostMeridiem($time->getHour()));

        $time->setTime(1);
        $this->assertSame(13, $time->to24HourClockPostMeridiem($time->getHour()));

        $time->setTime(11);
        $this->assertSame(23, $time->to24HourClockPostMeridiem($time->getHour()));

        $time->setTime(12);
        $this->assertSame(12, $time->to24HourClockPostMeridiem($time->getHour()));

        $time->setTime(13);
        $this->assertSame(13, $time->to24HourClockPostMeridiem($time->getHour()));

        $time->setTime(23);
        $this->assertSame(23, $time->to24HourClockPostMeridiem($time->getHour()));

        // 12-hour clock
        TimeGeneratorAbstract::getModuleSettings()->setHasTwentyFour(false);
        $this->assertFalse(TimeGeneratorAbstract::getModuleSettings()->getHasTwentyFour());

        // 12-hour clock → 24-hour clock A.M.
        $time = new TimeGeneratorNL();

        $time->setTime(1);
        $this->assertSame(1, $time->to24HourClockAnteMeridiem($time->getHour()));

        $time->setTime(11);
        $this->assertSame(11, $time->to24HourClockAnteMeridiem($time->getHour()));

        $time->setTime(12);
        $this->assertSame(0, $time->to24HourClockAnteMeridiem($time->getHour()));


        // 12-hour clock → 24-hour clock P.M.
        $time = new TimeGeneratorNL();
        $time->setTime(1);
        $this->assertSame(1, $time->getHour());
        $this->assertSame(13, $time->to24HourClockPostMeridiem($time->getHour()));

        $time->setTime(2);
        $this->assertSame(2, $time->getHour());
        $this->assertSame(14, $time->to24HourClockPostMeridiem($time->getHour()));

        $time->setTime(11);
        $this->assertSame(23, $time->to24HourClockPostMeridiem($time->getHour()));

        $time->setTime(12);
        $this->assertSame(12, $time->to24HourClockPostMeridiem($time->getHour()));
    }

    /**
     *
     */
    public function testDeltaSecondsNL()
    {
        // Initialize settings
        TimeGeneratorAbstract::setExerciseSettings();
        TimeGeneratorAbstract::setModuleSettings();

        // Enable seconds
        TimeGeneratorAbstract::getExerciseSettings()->setHasSeconds(true);
        $this->assertTrue(TimeGeneratorAbstract::getExerciseSettings()->getHasSeconds());

        // 24-hour clock
        TimeGeneratorAbstract::getModuleSettings()->setHasTwentyFour(true);
        $this->assertTrue(TimeGeneratorAbstract::getModuleSettings()->getHasTwentyFour());

        $time = new TimeGeneratorNL();
        $this->assertSame('00:00:00', $time->toTime());
        $time->deltaSeconds();
        $this->assertSame('00:00:00', $time->toTime());
        $time->deltaSeconds(0);
        $this->assertSame('00:00:00', $time->toTime());

        $time = new TimeGeneratorNL();
        $this->assertSame('00:00:00', $time->toTime());
        $time->deltaSeconds(1);
        $this->assertSame('00:00:01', $time->toTime());

        $time = new TimeGeneratorNL();
        $this->assertSame('00:00:00', $time->toTime());
        $time->deltaSeconds(1 * (24 * 3600));
        $this->assertSame('00:00:00', $time->toTime());

        $time = new TimeGeneratorNL();
        $this->assertSame('00:00:00', $time->toTime());
        $time->deltaSeconds(2 * (24 * 3600));
        $this->assertSame('00:00:00', $time->toTime());

        $time = new TimeGeneratorNL();
        $this->assertSame('00:00:00', $time->toTime());
        $time->deltaSeconds(-1);
        $this->assertSame('23:59:59', $time->toTime());

        $time = new TimeGeneratorNL();
        $this->assertSame('00:00:00', $time->toTime());
        $time->deltaSeconds(-1 * (24 * 3600));
        $this->assertSame('00:00:00', $time->toTime());

        $time = new TimeGeneratorNL();
        $this->assertSame('00:00:00', $time->toTime());
        $time->deltaSeconds(-2 * (24 * 3600));
        $this->assertSame('00:00:00', $time->toTime());

        $time = new TimeGeneratorNL();
        $time->setTime(23, 59, 59);
        $this->assertSame('23:59:59', $time->toTime());
        $time->deltaSeconds(1);
        $this->assertSame('00:00:00', $time->toTime());
        $time->deltaSeconds(1);
        $this->assertSame('00:00:01', $time->toTime());

        $time = new TimeGeneratorNL();
        $time->setTime(23, 59, 59);
        $this->assertSame('23:59:59', $time->toTime());
        $time->deltaSeconds(2 * (24 * 3600));
        $this->assertSame('23:59:59', $time->toTime());

        $time = new TimeGeneratorNL();
        $time->setTime(23, 59, 59);
        $this->assertSame('23:59:59', $time->toTime());
        $time->deltaSeconds(23 * 3600 + 59 * 60 + 59);
        $this->assertSame('23:59:58', $time->toTime());

        $time = new TimeGeneratorNL();
        $time->setTime(23, 59, 59);
        $this->assertSame('23:59:59', $time->toTime());
        $time->deltaSeconds(2 * (23 * 3600 + 59 * 60 + 59));
        $this->assertSame('23:59:57', $time->toTime());

        $time = new TimeGeneratorNL();
        $time->setTime(23, 59, 59);
        $this->assertSame('23:59:59', $time->toTime());
        $time->deltaSeconds(-2 * (23 * 3600 + 59 * 60 + 59));
        $this->assertSame('00:00:01', $time->toTime());

        // 12-hour clock
        TimeGeneratorAbstract::getModuleSettings()->setHasTwentyFour(false);
        $this->assertFalse(TimeGeneratorAbstract::getModuleSettings()->getHasTwentyFour());

        $time = new TimeGeneratorNL();
        $this->assertSame('12:00:00', $time->toTime());
        $time->deltaSeconds();
        $this->assertSame('12:00:00', $time->toTime());
        $time->deltaSeconds(0);
        $this->assertSame('12:00:00', $time->toTime());

        $time = new TimeGeneratorNL();
        $this->assertSame('12:00:00', $time->toTime());
        $time->deltaSeconds(1);
        $this->assertSame('12:00:01', $time->toTime());

        $time = new TimeGeneratorNL();
        $this->assertSame('12:00:00', $time->toTime());
        $time->deltaSeconds(1 * (12 * 3600));
        $this->assertSame('12:00:00', $time->toTime());

        // @todo

        $time = new TimeGeneratorNL();
        $this->assertSame('12:00:00', $time->toTime());
        $time->deltaSeconds(2 * (12 * 3600));
        $this->assertSame('12:00:00', $time->toTime());

        $time = new TimeGeneratorNL();
        $this->assertSame('12:00:00', $time->toTime());
        $time->deltaSeconds(-1);
        $this->assertSame('11:59:59', $time->toTime());

        $time = new TimeGeneratorNL();
        $this->assertSame('12:00:00', $time->toTime());
        $time->deltaSeconds(-1 * (12 * 3600));
        $this->assertSame('12:00:00', $time->toTime());

        $time = new TimeGeneratorNL();
        $this->assertSame('12:00:00', $time->toTime());
        $time->deltaSeconds(-2 * (12 * 3600));
        $this->assertSame('12:00:00', $time->toTime());

        $time = new TimeGeneratorNL();
        $time->setTime(11, 59, 59);
        $this->assertSame('11:59:59', $time->toTime());
        $time->deltaSeconds(1);
        $this->assertSame('12:00:00', $time->toTime());
        $time->deltaSeconds(1);
        $this->assertSame('12:00:01', $time->toTime());

        $time = new TimeGeneratorNL();
        $time->setTime(11, 59, 59);
        $this->assertSame('11:59:59', $time->toTime());
        $time->deltaSeconds(2 * (12 * 3600));
        $this->assertSame('11:59:59', $time->toTime());

        $time = new TimeGeneratorNL();
        $time->setTime(11, 59, 59);
        $this->assertSame('11:59:59', $time->toTime());
        $time->deltaSeconds(11 * 3600 + 59 * 60 + 59);
        $this->assertSame('11:59:58', $time->toTime());

        $time = new TimeGeneratorNL();
        $time->setTime(11, 59, 59);
        $this->assertSame('11:59:59', $time->toTime());
        $time->deltaSeconds(2 * (11 * 3600 + 59 * 60 + 59));
        $this->assertSame('11:59:57', $time->toTime());

        $time = new TimeGeneratorNL();
        $time->setTime(11, 59, 59);
        $this->assertSame('11:59:59', $time->toTime());
        $time->deltaSeconds(-2 * (11 * 3600 + 59 * 60 + 59));
        $this->assertSame('12:00:01', $time->toTime());

        $time = new TimeGeneratorNL();
        $time->setTime(1, 0, 0);
        $this->assertSame('01:00:00', $time->toTime());
        $time->deltaSeconds(-1);
        $this->assertSame('12:59:59', $time->toTime());
    }


    /**
     *
     */
    public function testDeltaMinutesNL()
    {
        // Initialize settings
        TimeGeneratorAbstract::setExerciseSettings();
        TimeGeneratorAbstract::setModuleSettings();

        // 24-hour clock
        TimeGeneratorAbstract::getModuleSettings()->setHasTwentyFour(true);
        $this->assertTrue(TimeGeneratorAbstract::getModuleSettings()->getHasTwentyFour());
        list ($hMin, $hMax) = TimeGeneratorAbstract::getHourRange();
        list ($mMin, $mMax) = TimeGeneratorAbstract::getMinuteRange();

        $time = new TimeGeneratorNL();
        $this->assertSame('00:00', $time->toTime());
        $time->deltaMinutes(1);
        $this->assertSame('00:01', $time->toTime());

        $time = new TimeGeneratorNL();
        $this->assertSame('00:00', $time->toTime());
        $time->deltaMinutes(30);
        $this->assertSame('00:30', $time->toTime());

        $time = new TimeGeneratorNL();
        $this->assertSame('00:00', $time->toTime());
        $time->deltaMinutes(60);
        $this->assertSame('01:00', $time->toTime());

        $time = new TimeGeneratorNL();
        $this->assertSame('00:00', $time->toTime());
        $time->deltaMinutes(-1);
        $this->assertSame('23:59', $time->toTime());

        $time = new TimeGeneratorNL();
        $this->assertSame('00:00', $time->toTime());
        $time->deltaMinutes(-30);
        $this->assertSame('23:30', $time->toTime());

        $time = new TimeGeneratorNL();
        $this->assertSame('00:00', $time->toTime());
        $time->deltaMinutes(-60);
        $this->assertSame('23:00', $time->toTime());

        // Positive addend
//        for ($m = $mMin; $m<= $mMax; ++$m) {
//            $time = new TimeGeneratorNL();
//            $this->assertSame('00:00', $time->toTime());
//            $time->deltaMinutes($h);
//            $this->assertSame(sprintf('%02d:00', $h), $time->toTime());
////            var_dump('00:00 + ' . $h . ' = ' . sprintf('%02d:00', $h) . ' = ' . $time->toTime());
//        }

        // 12-hour clock
        TimeGeneratorAbstract::getModuleSettings()->setHasTwentyFour(false);
        $this->assertFalse(TimeGeneratorAbstract::getModuleSettings()->getHasTwentyFour());


        list ($hMin, $hMax) = TimeGeneratorAbstract::getHourRange();
        list ($mMin, $mMax) = TimeGeneratorAbstract::getMinuteRange();

        $time = new TimeGeneratorNL();
        $this->assertSame('12:00', $time->toTime());
        $time->deltaMinutes(1);
        $this->assertSame('12:01', $time->toTime());

        $time = new TimeGeneratorNL();
        $this->assertSame('12:00', $time->toTime());
        $time->deltaMinutes(30);
        $this->assertSame('12:30', $time->toTime());

        $time = new TimeGeneratorNL();
        $this->assertSame('12:00', $time->toTime());
        $time->deltaMinutes(60);
        $this->assertSame('01:00', $time->toTime());

        $time = new TimeGeneratorNL();
        $this->assertSame('12:00', $time->toTime());
        $time->deltaMinutes(-1);
        $this->assertSame('11:59', $time->toTime());

        $time = new TimeGeneratorNL();
        $this->assertSame('12:00', $time->toTime());
        $time->deltaMinutes(-30);
        $this->assertSame('11:30', $time->toTime());

        $time = new TimeGeneratorNL();
        $this->assertSame('12:00', $time->toTime());
        $time->deltaMinutes(-60);
        $this->assertSame('11:00', $time->toTime());

        $time = new TimeGeneratorNL();
        $time->setTime(1);
        $this->assertSame('01:00', $time->toTime());
        $time->deltaMinutes(1);
        $this->assertSame('01:01', $time->toTime());

        $time = new TimeGeneratorNL();
        $time->setTime(1);
        $this->assertSame('01:00', $time->toTime());
        $time->deltaMinutes(30);
        $this->assertSame('01:30', $time->toTime());

        $time = new TimeGeneratorNL();
        $time->setTime(1);
        $this->assertSame('01:00', $time->toTime());
        $time->deltaMinutes(60);
        $this->assertSame('02:00', $time->toTime());

        $time = new TimeGeneratorNL();
        $time->setTime(1);
        $this->assertSame('01:00', $time->toTime());
        $time->deltaMinutes(-1);
        $this->assertSame('12:59', $time->toTime());

        $time = new TimeGeneratorNL();
        $time->setTime(1);
        $this->assertSame('01:00', $time->toTime());
        $time->deltaMinutes(-30);
        $this->assertSame('12:30', $time->toTime());

        $time = new TimeGeneratorNL();
        $time->setTime(1);
        $this->assertSame('01:00', $time->toTime());
        $time->deltaMinutes(-60);
        $this->assertSame('12:00', $time->toTime());

    }

    /**
     *
     */
    public function testDeltaHoursNL()
    {
        // Initialize settings
        TimeGeneratorAbstract::setExerciseSettings();
        TimeGeneratorAbstract::setModuleSettings();

        // 24-hour clock
        TimeGeneratorAbstract::getModuleSettings()->setHasTwentyFour(true);
        $this->assertTrue(TimeGeneratorAbstract::getModuleSettings()->getHasTwentyFour());
        list ($hMin, $hMax) = TimeGeneratorAbstract::getHourRange();

        // Positive addend
        for ($h = $hMin; $h <= $hMax; ++$h) {
            $time = new TimeGeneratorNL();
            $this->assertSame('00:00', $time->toTime());
            $time->deltaHours($h);
            $this->assertSame(sprintf('%02d:00', $h), $time->toTime());
        }

        $time = new TimeGeneratorNL();
        $this->assertSame('00:00', $time->toTime());
        $time->deltaHours(24);
        $this->assertSame(sprintf('00:00', $h), $time->toTime());

        $time = new TimeGeneratorNL();
        $this->assertSame('00:00', $time->toTime());
        $time->deltaHours(47);
        $this->assertSame(sprintf('23:00', $h), $time->toTime());

        // Negative addend
        for ($h = $hMin; $h <= $hMax; ++$h) {
            $time = new TimeGeneratorNL();
            $this->assertSame('00:00', $time->toTime());
            $time->deltaHours(-$h);
            $this->assertSame(sprintf('%02d:00', 0 < $h ? 24 - $h : 0), $time->toTime());
        }

        $time = new TimeGeneratorNL();
        $this->assertSame('00:00', $time->toTime());
        $time->deltaHours(-24);
        $this->assertSame(sprintf('00:00', $h), $time->toTime());

        $time = new TimeGeneratorNL();
        $this->assertSame('00:00', $time->toTime());
        $time->deltaHours(-47);
        $this->assertSame(sprintf('01:00', $h), $time->toTime());

        $time = new TimeGeneratorNL();
        $time->setTime(12);
        $this->assertSame('12:00', $time->toTime());
        $time->deltaHours(-25);
        $this->assertSame(sprintf('11:00', $h), $time->toTime());

        // 12-hour clock
        TimeGeneratorAbstract::getModuleSettings()->setHasTwentyFour(false);
        $this->assertFalse(TimeGeneratorAbstract::getModuleSettings()->getHasTwentyFour());
        list ($hMin, $hMax) = TimeGeneratorAbstract::getHourRange();

        $time = new TimeGeneratorNL();
        $this->assertSame('12:00', $time->toTime());
        $time->deltaHours(0);
        $this->assertSame(sprintf('12:00', $h), $time->toTime());

        for ($h = $hMin; $h <= $hMax; ++$h) {
            $time = new TimeGeneratorNL();
            $this->assertSame('12:00', $time->toTime());
            $time->deltaHours($h);
            $this->assertSame(sprintf('%02d:00', $h), $time->toTime());
        }

        $time = new TimeGeneratorNL();
        $this->assertSame('12:00', $time->toTime());
        $time->deltaHours(13);
        $this->assertSame(sprintf('01:00', $h), $time->toTime());

        $time = new TimeGeneratorNL();
        $this->assertSame('12:00', $time->toTime());
        $time->deltaHours(24);
        $this->assertSame(sprintf('12:00', $h), $time->toTime());

        for ($h = $hMin; $h <= $hMax; ++$h) {
            $time = new TimeGeneratorNL();
            $this->assertSame('12:00', $time->toTime());
            $time->deltaHours(-$h);
            $this->assertSame(sprintf('%02d:00', $h < 12 ? 12 - $h : 12), $time->toTime());
        }

        $time = new TimeGeneratorNL();
        $this->assertSame('12:00', $time->toTime());
        $time->deltaHours(-13);
        $this->assertSame(sprintf('11:00', $h), $time->toTime());

        $time = new TimeGeneratorNL();
        $this->assertSame('12:00', $time->toTime());
        $time->deltaHours(-23);
        $this->assertSame(sprintf('01:00', $h), $time->toTime());

        $time = new TimeGeneratorNL();
        $this->assertSame('12:00', $time->toTime());
        $time->deltaHours(-25);
        $this->assertSame(sprintf('11:00', $h), $time->toTime());

        $time = new TimeGeneratorNL();
        $time->setTime(1);
        $this->assertSame('01:00', $time->toTime());
        $time->deltaHours(-11);
        $this->assertSame(sprintf('02:00', $h), $time->toTime());

        $time = new TimeGeneratorNL();
        $time->setTime(2);
        $this->assertSame('02:00', $time->toTime());
        $time->deltaHours(-1);
        $this->assertSame(sprintf('01:00', $h), $time->toTime());

        $time = new TimeGeneratorNL();
        $time->setTime(1);
        $this->assertSame('01:00', $time->toTime());
        $time->deltaHours(-1);
        $this->assertSame(sprintf('12:00', $h), $time->toTime());
    }

    /**
     *
     */
    public function testGetHoursAvailableEN()
    {
        // Initialize settings
        TimeGeneratorAbstract::setExerciseSettings();
        TimeGeneratorAbstract::setModuleSettings();

        $time = new TimeGeneratorEN();

        // 24-hour clock
        $time::getModuleSettings()->setHasTwentyFour(true);
        $this->assertTrue($time::getModuleSettings()->getHasTwentyFour());
        $hours = $time->getHoursAvailable();

        $this->assertCount(24, $hours);
        $this->assertContains(0, $hours);
        $this->assertNotContains(24, $hours);

        // 12-hour clock
        $time::getModuleSettings()->setHasTwentyFour(false);
        $this->assertFalse($time::getModuleSettings()->getHasTwentyFour());
        $hours = $time->getHoursAvailable();
        $this->assertCount(12, $hours);
        $this->assertContains(12, $hours);
        $this->assertNotContains(0, $hours);
    }

    /**
     * Test TimeGeneratorAbstract getMinutesAvailable() method.
     */
    public function testMinutesAvailable()
    {
        // Initialize settings
        TimeGeneratorAbstract::setExerciseSettings();
        TimeGeneratorAbstract::setModuleSettings();

        $time = new TimeGeneratorEN();

        $time::getExerciseSettings()->setHasFullHour(false);
        $time::getExerciseSettings()->setHasQuarterPast(false);
        $time::getExerciseSettings()->setHasHalfHour(false);
        $time::getExerciseSettings()->setHasQuarterTo(false);
        $time::getExerciseSettings()->setMinutesIncrement(ExerciseSettings::MINUTES_INCREMENT_OFF);
        $this->assertFalse($time::getExerciseSettings()->getHasFullHour());
        $this->assertFalse($time::getExerciseSettings()->getHasQuarterPast());
        $this->assertFalse($time::getExerciseSettings()->getHasHalfHour());
        $this->assertFalse($time::getExerciseSettings()->getHasQuarterTo());
        $this->assertSame(ExerciseSettings::MINUTES_INCREMENT_OFF, $time::getExerciseSettings()->getMinutesIncrement());
        $this->assertCount(0, $time->getMinutesAvailable());

        // Full Hour
        $time::getExerciseSettings()->setHasFullHour(true);
        $this->assertTrue($time::getExerciseSettings()->getHasFullHour());
        $this->assertContains(0, $time->getMinutesAvailable());
        $time::getExerciseSettings()->setHasFullHour(false);
        $this->assertFalse($time::getExerciseSettings()->getHasFullHour());
        $this->assertNotContains(0, $time->getMinutesAvailable());

        // Quarter Past
        $time::getExerciseSettings()->setHasQuarterPast(true);
        $this->assertTrue($time::getExerciseSettings()->getHasQuarterPast());
        $this->assertContains(15, $time->getMinutesAvailable());
        $time::getExerciseSettings()->setHasQuarterPast(false);
        $this->assertFalse($time::getExerciseSettings()->getHasQuarterPast());
        $this->assertNotContains(15, $time->getMinutesAvailable());

        // Half Hour
        $time::getExerciseSettings()->setHasHalfHour(true);
        $this->assertTrue($time::getExerciseSettings()->getHasHalfHour());
        $this->assertContains(30, $time->getMinutesAvailable());
        $time::getExerciseSettings()->setHasHalfHour(false);
        $this->assertFalse($time::getExerciseSettings()->getHasHalfHour());
        $this->assertNotContains(30, $time->getMinutesAvailable());

        // Quarter To
        $time::getExerciseSettings()->setHasQuarterTo(true);
        $this->assertTrue($time::getExerciseSettings()->getHasQuarterTo());
        $this->assertContains(45, $time->getMinutesAvailable());
        $time::getExerciseSettings()->setHasQuarterTo(false);
        $this->assertFalse($time::getExerciseSettings()->getHasQuarterTo());
        $this->assertNotContains(45, $time->getMinutesAvailable());

        // @todo Should increments also contain Full Hour, Quarter Past, Half Hour and Quarter To?

        // Minutes Increment 1
        $time::getExerciseSettings()->setMinutesIncrement(ExerciseSettings::MINUTES_INCREMENT_01);
        $this->assertSame(ExerciseSettings::MINUTES_INCREMENT_01, $time::getExerciseSettings()->getMinutesIncrement());
        $this->assertCount(60, $time->getMinutesAvailable());

        // Minutes Increment 5
        $time::getExerciseSettings()->setMinutesIncrement(ExerciseSettings::MINUTES_INCREMENT_05);
        $this->assertSame(ExerciseSettings::MINUTES_INCREMENT_05, $time::getExerciseSettings()->getMinutesIncrement());
        $this->assertCount(12, $time->getMinutesAvailable());

        // Minutes Increment 10
        $time::getExerciseSettings()->setMinutesIncrement(ExerciseSettings::MINUTES_INCREMENT_10);
        $this->assertSame(ExerciseSettings::MINUTES_INCREMENT_10, $time::getExerciseSettings()->getMinutesIncrement());
        $this->assertCount(6, $time->getMinutesAvailable());
    }
}
