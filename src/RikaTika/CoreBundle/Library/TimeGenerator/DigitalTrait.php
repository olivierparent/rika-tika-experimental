<?php

namespace RikaTika\CoreBundle\Library\TimeGenerator;

/**
 * DigitalTrait
 *
 * @category RikaTika
 * @package CoreBundle
 * @subpackage Library\TimeGenerator
 * @author Olivier Parent <olivier.parent@arteveldehs.be>
 * @copyright Copyright © 2011-2014, Artevelde University College Ghent
 * @license http://www.clocklearning.org/LICENSE.txt
 */
trait DigitalTrait
{
    /**
     * Allow imaginary time formats for digital clock
     *
     * @var bool
     */
    protected $hasAllowImaginaryTimeFormat = false;


    /**
     * Set hasAllowImaginaryTimeFormat
     *
     * @param $bool
     * @return $this
     */
    public function setHasAllowImaginaryTimeFormat($bool)
    {
        $this->hasAllowImaginaryTimeFormat = $bool;

        return $this;
    }

    /**
     * Get hasAllowImaginaryTimeFormat
     *
     * @return bool
     */
    public function getHasAllowImaginaryTimeFormat()
    {
        return $this->hasAllowImaginaryTimeFormat;
    }


    /**
     * Generate answer ERROR_DIGITAL_EARLIER_BY_30M
     *
     * @return TimeGeneratorAbstract
     */
    public function errorDigitalEarlierBy30M()
    {
        $this->errorAnalogueHoursHalfHourEarlier();

        return $this->setAnswerMode(__FUNCTION__);
    }

    /**
     * Generate answer ERROR_DIGITAL_EARLIER_BY_60M
     *
     * @return TimeGeneratorAbstract
     */
    public function errorDigitalEarlierBy60M()
    {
        $this->errorAnalogueHoursHourEarlier();

        return $this->setAnswerMode(__FUNCTION__);
    }

    /**
     * Generate answer ERROR_DIGITAL_READ_H×10
     *
     * @return bool
     */
    public function errorDigitalReadH×10()
    {
        return $this->setAnswerMode(__FUNCTION__);
    }

    /**
     * Generate answer ERROR_DIGITAL_READ_H×100
     *
     * @return bool
     */
    public function errorDigitalReadH×100()
    {
        return $this->setAnswerMode(__FUNCTION__);
    }
    /**
     * Generate answer ERROR_DIGITAL_READ_HOUR
     *
     * @return bool
     */
    public function errorDigitalReadHour()
    {
        return $this->setAnswerMode(__FUNCTION__);
    }

    /**
     * Generate answer ERROR_DIGITAL_READ_M00_AS_0
     *
     * @return bool
     */
    public function errorDigitalReadM00As0()
    {
        return $this->setAnswerMode(__FUNCTION__);
    }

    /**
     * Generate answer ERROR_DIGITAL_READ_REVERSE_M_DIGITS
     *
     * @return bool
     */
    public function errorDigitalReadReverseMDigits()
    {
        return $this->setAnswerMode(__FUNCTION__);
    }

    /**
     * Generate answer ERROR_DIGITAL_READ_QUARTERS_AS_NUMBER
     *
     * @return bool
     */
    public function errorDigitalReadQuartersAsNumber()
    {
        return $this->setAnswerMode(__FUNCTION__);
    }

    /**
     * Generate answer ERROR_DIGITAL_READ_SWAP_H_AND_M
     *
     * @return bool
     */
    public function errorDigitalReadSwapHAndM()
    {
        return $this->setAnswerMode(__FUNCTION__);
    }

    /**
     * Generate answer ERROR_DIGITAL_READ_SWAP_H_AND_M_THEN_PAST
     *
     * @return bool
     */
    public function errorDigitalReadSwapHAndMThenPast()
    {
        return $this->setAnswerMode(__FUNCTION__);
    }

    /**
     * Generate answer ERROR_DIGITAL_READ_SWAP_H_AND_M_THEN_REVERSE_H_DIGITS
     *
     * @return bool
     */
    public function errorDigitalReadSwapHAndMThenReverseHDigits()
    {
        return $this->setAnswerMode(__FUNCTION__);
    }

    /**
     * Generate answer ERROR_DIGITAL_READ_TO
     *
     * @return bool
     */
    public function errorDigitalReadTo()
    {
        return $this->setAnswerMode(__FUNCTION__);
    }

    /**
     * Generate answer ERROR_DIGITAL_SWAP_PAST_AND_TO
     *
     * @return bool
     */
    public function errorDigitalSwapPastAndTo()
    {
        $this->errorAnalogueMinutesMirrorY();

        return $this->setAnswerMode(__FUNCTION__);
    }

    /**
     * Generate answer ERROR_DIGITAL_SWAP_PAST_AND_TO_THEN_READ_QUARTERS_AS_NUMBER
     *
     * @return bool
     */
    public function errorDigitalSwapPastAndToThenReadQuartersAsNumber()
    {
        $this->errorDigitalSwapPastAndTo();

        return $this->setAnswerMode(__FUNCTION__);
    }
}
