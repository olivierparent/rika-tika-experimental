<?php

namespace RikaTika\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Member
 *
 * @category RikaTika
 * @package CoreBundle
 * @subpackage Entity
 * @author Olivier Parent <olivier.parent@arteveldehs.be>
 * @copyright Copyright © 2011-2014, Artevelde University College Ghent
 * @license http://www.clocklearning.org/LICENSE.txt
 *
 * @ORM\Table(name="users_members")
 * @ORM\Entity(repositoryClass="RikaTika\CoreBundle\Entity\MemberRepository")
 */
class Member extends UserAbstract
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var MemberGroup
     *
     * @ORM\ManyToOne(targetEntity="MemberGroup", inversedBy="members")
     * @ORM\JoinColumn(name="user_group_id", nullable=false)
     */
    private $memberGroup;

    /**
     * @var ExerciseSettings
     *
     * @ORM\ManyToOne(targetEntity="ExerciseSettings")
     * @ORM\JoinColumn(name="exercise_settings_id")
     */
    private $exerciseSettings;

    /**
     * @var ModuleSettings
     *
     * @ORM\ManyToOne(targetEntity="ModuleSettings")
     * @ORM\JoinColumn(name="module_settings_id")
     */
    private $moduleSettings;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="birthday", type="date")
     */
    private $birthday;

    /**
     * @var array
     */
    protected $roles = [self::ROLE_MEMBER];

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set member group
     *
     * @param MemberGroup $memberGroup
     * @return Member
     */
    public function setMemberGroup(MemberGroup $memberGroup)
    {
        $this->memberGroup = $memberGroup;

        return $this;
    }

    /**
     * Get member group
     *
     * @return MemberGroup
     */
    public function getMemberGroup()
    {
        return $this->memberGroup;
    }

    /**
     * Set exercise settings
     *
     * @param ExerciseSettings $exerciseSettings
     * @return Member
     */
    public function setExerciseSettings(ExerciseSettings $exerciseSettings)
    {
        $this->exerciseSettings = $exerciseSettings;

        return $this;
    }

    /**
     * Get exercise settings
     *
     * @return ExerciseSettings
     */
    public function getExerciseSettings()
    {
        return $this->exerciseSettings;
    }

    /**
     * Set module settings
     *
     * @param ModuleSettings $moduleSettings
     * @return Member
     */
    public function setModuleSettings(ModuleSettings $moduleSettings)
    {
        $this->moduleSettings = $moduleSettings;

        return $this;
    }

    /**
     * Get module settings
     *
     * @return ModuleSettings
     */
    public function getModuleSettings()
    {
        return $this->moduleSettings;
    }

    /**
     * Set birthday
     *
     * @param \DateTime $birthday
     * @return Member
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;

        return $this;
    }

    /**
     * Get birthday
     *
     * @return \DateTime
     */
    public function getBirthday()
    {
        return $this->birthday;
    }
}
