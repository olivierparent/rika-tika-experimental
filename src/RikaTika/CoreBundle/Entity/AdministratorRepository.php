<?php

namespace RikaTika\CoreBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * AdministratorRepository
 *
 * @category RikaTika
 * @package CoreBundle
 * @subpackage Entity
 * @author Olivier Parent <olivier.parent@arteveldehs.be>
 * @copyright Copyright © 2011-2014, Artevelde University College Ghent
 * @license http://www.clocklearning.org/LICENSE.txt
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class AdministratorRepository extends EntityRepository
{
}
