<?php

namespace RikaTika\CoreBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * MemberGroup
 *
 * @category RikaTika
 * @package CoreBundle
 * @subpackage Entity
 * @author Olivier Parent <olivier.parent@arteveldehs.be>
 * @copyright Copyright © 2011-2014, Artevelde University College Ghent
 * @license http://www.clocklearning.org/LICENSE.txt
 *
 * @ORM\Table(name="member_groups")
 * @ORM\Entity(repositoryClass="RikaTika\CoreBundle\Entity\MemberGroupRepository")
 */
class MemberGroup
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Supervisor
     *
     * @ORM\ManyToOne(targetEntity="Supervisor")
     * @ORM\JoinColumn(nullable=false)
     */
    private $supervisor;

    /**
     * @var ExerciseSettings
     *
     * @ORM\ManyToOne(targetEntity="ExerciseSettings")
     * @ORM\JoinColumn(name="exercise_settings_id")
     */
    private $exerciseSettings;

    /**
     * @var ModuleSettings
     *
     * @ORM\ManyToOne(targetEntity="ModuleSettings")
     * @ORM\JoinColumn(name="module_settings_id")
     */
    private $moduleSettings;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Member", mappedBy="memberGroup")
     */
    private $members;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=false, options={"default" = true})
     */
    private $isActive = true;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_deleted", type="boolean", nullable=false, options={"default" = false})
     */
    private $isDeleted = false;


    public function __construct()
    {
        $this->members = new ArrayCollection();
        $this->createdAt = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set supervisor
     *
     * @param Supervisor $supervisor
     * @return MemberGroup
     */
    public function setSupervisor(Supervisor $supervisor)
    {
        $this->supervisor = $supervisor;

        return $this;
    }

    /**
     * Get supervisor
     *
     * @return Supervisor
     */
    public function getSupervisor()
    {
        return $this->supervisor;
    }

    /**
     * Set exercise settings
     *
     * @param ExerciseSettings $exerciseSettings
     * @return MemberGroup
     */
    public function setExerciseSettings(ExerciseSettings $exerciseSettings)
    {
        $this->exerciseSettings = $exerciseSettings;

        return $this;
    }

    /**
     * Get exercise settings
     *
     * @return ExerciseSettings
     */
    public function getExerciseSettings()
    {
        return $this->exerciseSettings;
    }

    /**
     * Set module settings
     *
     * @param ModuleSettings $moduleSettings
     * @return MemberGroup
     */
    public function setModuleSettings(ModuleSettings $moduleSettings)
    {
        $this->moduleSettings = $moduleSettings;

        return $this;
    }

    /**
     * Get module settings
     *
     * @return ModuleSettings
     */
    public function getModuleSettings()
    {
        return $this->moduleSettings;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return MemberGroup
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return MemberGroup
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     * @return MemberGroup
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean 
     */
    public function getIsActive()
    {
        return $this->isActive;
    }


    /**
     * Set isDeleted
     *
     * @param boolean $isDeleted
     * @return MemberGroup
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return boolean
     */
    public function getIsDeleted()
    {
        return $this->$isDeleted;
    }
}
