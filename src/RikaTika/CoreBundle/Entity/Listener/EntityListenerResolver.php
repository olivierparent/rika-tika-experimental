<?php

namespace RikaTika\CoreBundle\Entity\Listener;

use Doctrine\ORM\Mapping\DefaultEntityListenerResolver;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * EntityListenerResolver
 *
 * @category RikaTika
 * @package CoreBundle
 * @subpackage Entity\Listener
 * @author Olivier Parent <olivier.parent@arteveldehs.be>
 * @copyright Copyright © 2011-2014, Artevelde University College Ghent
 * @license http://www.clocklearning.org/LICENSE.txt
 */
class EntityListenerResolver extends DefaultEntityListenerResolver
{
    /**
     * @var \Symfony\Component\DependencyInjection\ContainerInterface
     */
    private $container;

    /**
     * @var array
     */
    private $mapping;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->mapping = [];
    }

    /**
     * @param $className
     * @param $service
     */
    public function addMapping($className, $service)
    {
        $this->mapping[$className] = $service;
    }

    /**
     * @param string $className
     * @return object
     */
    public function resolve($className)
    {
        if (isset($this->mapping[$className]) && $this->container->has($this->mapping[$className])) {
            return $this->container->get($this->mapping[$className]);
        }

        return parent::resolve($className);
    }
}
