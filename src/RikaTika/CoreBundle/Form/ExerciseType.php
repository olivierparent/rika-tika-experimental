<?php

namespace RikaTika\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ExerciseType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('createdAt')
            ->add('questions')
            ->add('answers')
            ->add('modes')
            ->add('isCorrect')
            ->add('startedAt')
            ->add('endedAt')
            ->add('exerciseType')
            ->add('exerciseSet')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'RikaTika\CoreBundle\Entity\Exercise'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'rikatika_corebundle_exercise';
    }
}
