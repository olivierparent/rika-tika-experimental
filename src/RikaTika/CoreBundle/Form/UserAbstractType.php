<?php

namespace RikaTika\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserAbstractType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName')
            ->add('lastName')
            ->add('username')
            ->add('password')
            ->add('createdAt')
            ->add('activatedAt')
            ->add('deletedAt')
            ->add('language')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'RikaTika\CoreBundle\Entity\UserAbstract'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'rikatika_corebundle_userabstract';
    }
}
